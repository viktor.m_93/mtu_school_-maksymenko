import { assert } from "chai";
import ApiController from "../../src/controller/ApiController";
import { positive, negative } from "../../src/data/Fixtures/GetPet";
import petChecker from "../../src/cheker/PetChecker";

describe("Find Pet by ID. Positive", function () {
  positive.forEach(({ name, RouteData, ExpectedData }) => (
    it(this.description + name, async () => {
      const search = new ApiController();
      const { body } = await search.getOnePet(RouteData!);
      petChecker.getPetZod.parse(body);
      assert.deepEqual(body, ExpectedData?.body, "Response body"); // проверка соответствия полученного 'body' ожидаемому
    })
  ));
});
describe("Find Pet by ID. Negative", function () {
  negative.forEach(({ name, RouteData, ExpectedData }) => (
    it(this.description + name, async () => {
      const search = new ApiController();
      const { body, statusCode } = await search.getOnePetError(RouteData!);
      petChecker.errorPetZod.parse(body);
      assert.equal(statusCode, ExpectedData?.status, "Status Code");
      assert.deepEqual(body, ExpectedData?.body, "Response body");
    })
  ));
});
