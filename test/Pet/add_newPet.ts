import { assert } from "chai";
import ApiController from "../../src/controller/ApiController";
import { positive, negative } from "../../src/data/Fixtures/NewPet";
import PetChecker from "../../src/cheker/PetChecker";

//
describe("Create new Pet. Positive", function () {
  positive.forEach(({ name, SendData }) => (
    it(this.description + name, async () => {
      const pet = new ApiController();
      const { body } = await pet.addNewPet(SendData!);
      PetChecker.getPetZod.parse(body);
      assert.deepEqual(body, SendData, "Response body");
    })
  ));
});

describe("Create new Pet. Negative", function () {
  negative.forEach(({ name, SendData, ExpectedData }) => (
    it(this.description + name, async () => {
      const Pet = new ApiController();
      const { body, statusCode } = await Pet.addWrongPet(SendData!);
      PetChecker.errorPetZod.parse(body);
      assert.equal(statusCode, ExpectedData?.status, "Status Code");
      assert.deepEqual(body, ExpectedData?.body, "Error body");
    })
  ));
});
