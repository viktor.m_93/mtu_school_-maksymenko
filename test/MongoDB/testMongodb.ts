import { assert } from "chai";

const COLLECTION = "Test_Collection_Mongo";

type SendRecord = {
  id: number,
  name: string,
  gender: string,
  age: number,
};

const oneRecord: SendRecord = {
  id: 17,
  name: "Viktor",
  gender: "Male",
  age: 28,
};

const twoRecord: SendRecord[] = [
  {
    id: 19,
    name: "Masha",
    gender: "Female",
    age: 22,
  },
  {
    id: 26,
    name: "Alexandra",
    gender: "Female",
    age: 28,
  },
];

describe("Test Mongo Base Methods", () => {
  it("Make One Record", async () => {
    await global.mongo.createOne(COLLECTION, oneRecord);
    const record = await global.mongo.shouldBeOneRecord(COLLECTION, { id: oneRecord.id });
    assert.deepEqual(record, oneRecord, "One Record");
  });
  it("Make Two Record", async () => {
    await global.mongo.createMany(COLLECTION, twoRecord);
    const record = await global.mongo.getAllRecords(COLLECTION, { gender: "Female" });
    assert.deepEqual(record, twoRecord, "Two Record");
  });
  it("Read one record", async () => {
    const record = await global.mongo.getOnlyOneRecord(COLLECTION, { age: 28 });
    assert.deepEqual(record, oneRecord, "Read one record");
  });
});
