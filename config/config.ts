import { Helper } from "school-helper/lib/helper";
import { GeneralNS } from "../src/namespace/GeneralNS";
import ConfigType = GeneralNS.ConfigType;
import { LocalConfig } from "./local.config";

export const config: ConfigType = Helper.defaultsDeep(LocalConfig, {
  baseUrl: "http://localhost/",
  timeout: 10000,
  mongo: {
    connectionUrl: "mongodb://localhost:27017/defaultDB",
    options: {
    },
  },
});
