import { MongoComponent } from "school-helper/lib/component";
import { config } from "./config";

const { mongo: { connectionUrl = "", options = {} } = {} } = config;

declare global {
  namespace NodeJS {
    interface Global {
      mongo: MongoComponent,
    }
  }
}

global.mongo = new MongoComponent(connectionUrl, options);
jest.setTimeout(config.timeout);
