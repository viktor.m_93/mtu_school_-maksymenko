import { BaseController, ControllerOptions } from "school-helper/lib/controller";
import { config } from "../../config/config";
import { createPetNS, getPetNS } from "../namespace/PetNS";

export default class ApiController extends BaseController {
  private addPath: string = "v2/pet";

  constructor(options: ControllerOptions = { baseUrl: config.baseUrl }) {
    super(options);
  }

  async getOnePet({ id }: getPetNS.RouteData) {
    return this.request
      .get<getPetNS.ReceivedData>(`${this.addPath}/${id}`);
  }

  async getOnePetError({ id }: getPetNS.ErrorRouteData) {
    return this.request
      .getError<getPetNS.ReceivedErrorData>(`${this.addPath}/${id}`);
  }

  async addNewPet(data: createPetNS.SendData) {
    return this.request
      .json(data)
      .post<createPetNS.SendData>(this.addPath);
  }

  async addWrongPet(data: createPetNS.SendDataError) {
    return this.request
      .json(data)
      .postError<createPetNS.ReceivedErrorPet>(this.addPath);
  }
}
