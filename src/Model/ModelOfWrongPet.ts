import * as faker from "faker";
import BaseModel from "./BaseModel";
import { createPetNS } from "../namespace/PetNS";
import PetChecker from "../cheker/PetChecker";

export default class CreateWrongPetModel extends BaseModel<createPetNS.SendDataError> {
  get defaultModel(): createPetNS.SendDataError {
    return {
      id: faker.datatype.number(30),
      category: {
        id: faker.datatype.number(),
        name: faker.random.arrayElement([3, 5, 8]),
      },
      name: faker.lorem.word(),
      photoUrls: [
        faker.internet.url(),
      ],
      tags: [
        {
          id: faker.datatype.number(),
          name: faker.lorem.word(),
        },
      ],
      status: faker.random.arrayElement(PetChecker.getPetZod.shape.status.options),
    };
  }
}
