import { unset, cloneDeep } from "lodash";

export default abstract class BaseModel<T extends Record<string, unknown>> {
  protected _result?: T;

  assign(data: Partial<T>): this {
    this._result = cloneDeep({ ...this.result, ...data });
    return this;
  }

  removeProps(...props: (keyof T)[]): Partial<T> {
    const result = cloneDeep(this.result);
    props.forEach((property) => {
      unset(result, property);
    });
    return result;
  }

  extract(): T {
    return cloneDeep(this.result);
  }

  protected get result(): T {
    if (!this._result) {
      this._result = cloneDeep(this.defaultModel);
    }
    return this._result;
  }

  get keys(): (keyof T)[] {
    return Object.keys(this.defaultModel);
  }

  protected abstract get defaultModel(): T;
}
