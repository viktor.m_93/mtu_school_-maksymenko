import * as faker from "faker";
import BaseModel from "./BaseModel";
import { createPetNS } from "../namespace/PetNS";
import PetChecker from "../cheker/PetChecker";

export default class CreatePetModel extends BaseModel<createPetNS.SendData> {
  get defaultModel(): createPetNS.SendData {
    return {
      id: faker.datatype.number(10),
      category: {
        id: faker.datatype.number(),
        name: faker.random.arrayElement(["cats", "dog", "fish"]),
      },
      name: faker.lorem.word(),
      photoUrls: [
        faker.internet.url(),
      ],
      tags: [
        {
          id: faker.datatype.number(),
          name: faker.lorem.word(),
        },
      ],
      status: faker.random.arrayElement(PetChecker.getPetZod.shape.status.options),
    };
  }
}
