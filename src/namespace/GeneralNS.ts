import { MongoClientOptions } from "mongodb";

export namespace GeneralNS {
  export type ConfigType = {
    baseUrl: string,
    timeout: number,
    mongo?: {
      connectionUrl: string,
      options?: MongoClientOptions,
    },

  };

  export type PositiveTemplate<R, S, B> = {
    name: string,
    RouteData?: R,
    SendData?: S,
    ExpectedData?: {
      status: number,
      body: B,
    },
  };

  export type NegativeTemplate<R, S, E> = {
    name: string,
    RouteData?: R,
    SendData?: S,
    ExpectedData?: {
      status: number,
      body: E,
    }
  };
}
