import * as zod from "zod";
import PetChecker from "../cheker/PetChecker";
import { GeneralNS } from "./GeneralNS";
import PositiveTemplate = GeneralNS.PositiveTemplate;
import NegativeTemplate = GeneralNS.NegativeTemplate;

export namespace getPetNS {
  export type RouteData = {
    id: number,
  };
  export type ErrorRouteData = {
    id: string | boolean | null | number
  };

  export type ReceivedData = zod.infer<typeof PetChecker.getPetZod>;
  export type ReceivedErrorData = zod.infer<typeof PetChecker.errorPetZod>;
  export type PositiveFixture = PositiveTemplate<RouteData, undefined, any>;
  export type NegativeFixture = NegativeTemplate<ErrorRouteData, undefined, ReceivedErrorData>;
}
export namespace createPetNS {
  export type SendData = zod.infer<typeof PetChecker.getPetZod>;
  export type SendDataError = Partial<Record<keyof SendData, any>>;
  export type ReceivedErrorPet = zod.infer<typeof PetChecker.errorPetZod>;
  export type PositiveFixture = PositiveTemplate<any, SendData, undefined>;
  export type NegativeFixture = NegativeTemplate<any, SendDataError, ReceivedErrorPet>;
}

