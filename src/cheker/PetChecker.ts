import * as zod from "zod";

export default class PetChecker {
  static get getPetZod() {
    return zod.object({
      id: zod.number().nonnegative(),
      category: zod.object(
        {
          id: zod.number().nonnegative(),
          name: zod.string().nonempty(),
        }
      ),
      name: zod.string().nonempty(),
      photoUrls: zod.array(zod.string().nonempty()).nonempty(),
      tags: zod.array(zod.object(
        {
          id: zod.number().nonnegative(),
          name: zod.string().nonempty(),
        }
      )).nonempty(),
      status: zod.enum(["available", "sold", "pending"]),
    });
  }

  static get errorPetZod() {
    return zod.object({
      code: zod.number().nonnegative(),
      type: zod.string().nonempty(),
      message: zod.string().nonempty(),
    });
  }
}
