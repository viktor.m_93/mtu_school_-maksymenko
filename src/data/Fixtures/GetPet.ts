import { getPetNS } from "../../namespace/PetNS";

export const positive: getPetNS.PositiveFixture[] = [
  {
    name: " Real Id",
    RouteData: {
      id: 23,
    },
    ExpectedData: {
      status: 200,
      body: {},
    },
  },
];
export const negative: getPetNS.NegativeFixture[] = [
  {
    name: " Invalid Id",
    RouteData: {
      id: 33333333,
    },
    ExpectedData: {
      status: 404,
      body: {
        code: 1,
        type: "error",
        message: "Pet not found",
      },
    },
  },
  {
    name: " Id as a string",
    RouteData: {
      id: "dog",
    },
    ExpectedData: {
      status: 404,
      body: {
        code: 404,
        type: "unknown",
        message: "Error id. Id must be a number",
      },
    },
  },
  {
    name: " Bolean Id",
    RouteData: {
      id: true,
    },
    ExpectedData: {
      status: 404,
      body: {
        code: 404,
        type: "unknown",
        message: "Error id. Id must be a number",
      },
    },
  },
  {
    name: " ID null",
    RouteData: {
      id: null,
    },
    ExpectedData: {
      status: 404,
      body: {
        code: 404,
        type: "unknown",
        message: "Error id. Id must be a number",
      },
    },
  },
];
