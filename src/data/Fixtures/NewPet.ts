import { createPetNS } from "../../namespace/PetNS";
import CreatePetModel from "../../Model/ModelOfPet";
import CreateWrongPetModel from "../../Model/ModelOfWrongPet";

export const positive: createPetNS.PositiveFixture[] = [
  {
    name: "Valid pet - random data",
    SendData: new CreatePetModel().extract(),
  },
  {
    name: "Valid pet - another name",
    SendData: new CreatePetModel().assign({ name: "Sam" }).extract(),
  },
  {
    name: "Valid pet - new name of tags",
    SendData: new CreatePetModel().assign({
      tags: [{
        id: 3,
        name: "wild",
      },
      ],
    }).extract(),

  },
];
export const negative: createPetNS.NegativeFixture[] = [
  {
    name: "Without ID",
    SendData: new CreateWrongPetModel().removeProps("id"),
    ExpectedData: {
      status: 500,
      body: {
        code: 500,
        type: "error",
        message: "something went wrong, please type ID number of pet",
      },
    },
  },
  {
    name: "Invalid name of category - number",
    SendData: new CreateWrongPetModel().assign({
      category: {
        id: 3,
        name: 3,
      },
    }).extract(),
    ExpectedData: {
      status: 500,
      body: {
        code: 500,
        type: "Error",
        message: "Something went wrong. Name of categories not a number",
      },
    },
  },
  {
    name: "Invalid status - boolean",
    SendData: new CreateWrongPetModel().assign({
      status: true,
    }).extract(),
    ExpectedData: {
      status: 500,
      body: {
        code: 500,
        type: "unknown",
        message: "something went wrong. Status must be ",
      },
    },
  },
];
